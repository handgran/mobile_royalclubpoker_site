
$(function(){

	setTimeout(function(){ 
		$(".loading").slideUp();
	 }, 3000);

	$(".modalRegras").click(function(e){
	 	$('#modalRegras').fadeIn();
	});
	$("#modalRegras").click(function(e){
	 	$(this).fadeOut();
	});

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			center:true,
			animateOut: 'fadeOut',
		});
	var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');
	$('#carrosselDestaqueLeft').click(function(){ carrosselDestaque.prev(); });
	$('#carrosselDestaqueRight').click(function(){ carrosselDestaque.next(); });

});
